﻿using System.Net;

namespace NetmqApp
{
    internal class MessageReceiver
    {
        private IPAddress loopback;
        private TransportType tcp;
        private int v;

        public MessageReceiver(IPAddress loopback, TransportType tcp, int v)
        {
            this.loopback = loopback;
            this.tcp = tcp;
            this.v = v;
        }
    }
}