﻿using System.Net;

namespace NetmqApp
{
    internal class MessageSender
    {
        private IPAddress loopback;
        private TransportType tcp;
        private int v;

        public MessageSender(IPAddress loopback, TransportType tcp, int v)
        {
            this.loopback = loopback;
            this.tcp = tcp;
            this.v = v;
        }
    }
}