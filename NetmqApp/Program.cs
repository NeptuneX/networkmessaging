﻿using System;
using static System.Console;


using System.Net;
using NetworkChannel;
using System.Threading;
using System.Linq;
using NetworkMessaging.Common;
using NetworkChannel.Common;
using System.Collections.Generic;

namespace ServerApp
{
    class Server
    {
        static void Main(string[] args)
        {
            WriteLine("Listening for the clients...");

            TestSignal();
            //TestData();
        }

        private static void TestData()
        {
                using (var reader = new NetworkDataChannel<QuickBinarySerializer>(new IPEndPoint(IPAddress.Loopback, 4040)))
                {
                while (true)
                {
                    var data = reader.ReceiveData(TimeSpan.FromMilliseconds(400));

                    if (data.Any())
                    {
                        foreach (var d in data)
                        {
                            var list = d.Data.Select(x => x as Person).ToList();
                            WriteLine($"Sent by {d.Sender.ToString()}  ... {d.Data.Length}");
                        }

                    }
                } 
            }
        }
        private static void TestSignal()
        {
            using (var reader = new NetworkSignalChannel(new IPEndPoint(IPAddress.Loopback, 4040)))

            {
                var escapePressed = new UntilTheEvent(() => Keyboard.KeyPressed().Key == ConsoleKey.Escape);

                var max = int.MinValue;
                while (!escapePressed.Happened)
                {
                    var delay = TimeSpan.FromMilliseconds(1);
                    var signals = reader.ReceiveSignal(delay).ToList();
                    if (signals.Any())
                    {

                        max = Math.Max(max, signals.Count);
                        WriteLine($"max: {max}");
                        foreach (var signal in signals)
                        {
                            switch (signal.Command)
                            {
                                case 2:
                                    reader.SendSignal(signal.Sender, 4, new object[] { "Level 1" });
                                    break;
                                case 4:
                                    reader.SendSignal(signal.Sender, 8, new object[] { "Level 2" });
                                    break;
                                case 16:
                                    reader.SendSignal(signal.Sender, 32, new object[] { "Level 3" });
                                    break;
                                default:
                                    reader.SendSignal(signal.Sender, -1);
                                    reader.SendSignal(987);
                                    break;
                            }
                        }
                    }



                }
            }
        }
    }

    [Serializable]
    public class Human
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

}
