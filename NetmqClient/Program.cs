﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using NetworkChannel;
using NetworkChannel.Common;
using NetworkMessaging.Common;
using static System.Console;

namespace ClientApp
{
    class Client
    {
        static void Main(string[] args)
        {
            var port = Keyboard.Ask<int>("Port (1000 - 65000):");

            //TestSignals(port);
            //TestData(port);
            TestWeird(port);
        }

        private static void TestWeird(int port)
        {
            using(var weird = new WeirdChannel(new IPEndPoint(IPAddress.Loopback, port)))
            {
                weird.SignalReceived += Weird_SignalReceived;

                var target = new IPEndPoint(IPAddress.Loopback, 4040);
                weird.Connect(target);

                for (var i = 0; i < 1000; i++)
                {
                    weird.Send(target, 234, "hello World");
                    Thread.Sleep(250);
                }





                while(true)
                    weird.Receive();
            }
        }

        private static void Weird_SignalReceived(object sender, SignalEventArgs e)
        {
            WriteLine($"{e.Count}, {e.Messages.First().Command}");
        }

        private static void TestData(int port)
        {
            using(var writer = new NetworkDataChannel<QuickBinarySerializer>(new IPEndPoint(IPAddress.Loopback, port), new IPEndPoint(IPAddress.Loopback, 4040)))
            {
                var ser = new QuickBinarySerializer();
                while (true)
                {
                    var list = new Person[50000];
                    for(var i = 0; i < list.Length; i++)
                    {
                        list[i] = new Person { Id = i, Name = "Kevin State" };
                    }

                    //var data = ser.ToBytes< Person >(list);
                    //var ret = ser.ToObjects<Person>(data);
                    writer.SendData(list);

                    Thread.Sleep(500);
                }
            }
        }
        private static void TestSignals(int port)
        {
            using (var writer = new NetworkSignalChannel(new IPEndPoint(IPAddress.Loopback, port), new IPEndPoint(IPAddress.Loopback, 4040)))
            {
                var escapePressed = new UntilTheEvent(() => Keyboard.KeyPressed().Key == ConsoleKey.Escape);
                while (!escapePressed.Happened)
                {
                    Console.Write("\nCommand: ");
                    for (int i = 0; i < 10000; i++)
                    {
                        var cmd = 2;// int.Parse(Console.ReadLine());
                        writer.SendSignal(cmd);
                        writer.SendSignal(cmd, 34567, "Hello World");
                    }

                    //writer.SendData(new string[] { "Hello", "World" });


                    var delay = TimeSpan.FromMilliseconds(50);
                    var signals = writer.ReceiveSignal(delay).ToList();
                    WriteLine($"received {signals.Count}");
                }
            }
        }
    }


}
