﻿using System;
using System.Diagnostics.Contracts;
using System.Net;
using NetworkChannel.Common;
using NetMQ.Sockets;
using System.Text;

namespace NetworkChannel
{
    public class ChannelReader<TSerializer> : NetworkChannel<TSerializer>
        where TSerializer : IObjectSerializer, new()
    {
        public ChannelReader(IPEndPoint endPoint):base(endPoint)
        {
            Contract.Requires<ArgumentNullException>(endPoint!=null);
            Contract.Ensures(_control!=null);
            Contract.Ensures(_data!=null);


            _control = new ChannelSocket(()=>new RouterSocket(), Host.ControlString);
            _control.Bind(Host.Control);

            _data = new ChannelSocket(() => new PullSocket(), Host.DataString);
            _data.Bind(Host.Data);
        }

        public override string SendData(object[] args) 
        {
            throw new InvalidOperationException("cannot send data from a NetworkChannelReader");
        }
        public override void SendData(string target, object[] args)
        {
            throw new InvalidOperationException("cannot send data from a NetworkChannelReader");
        }
        public override void SendSignal(int command, object[] args)
        {
            throw new InvalidOperationException("Cannot send command to the connected source without specifying the address");
        }
    }
}
