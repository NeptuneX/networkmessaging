﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Net;
using System.Text;
using NetMQ;
using NetMQ.Sockets;
using NetworkChannel.Common;

namespace NetworkChannel
{

    public enum SocketType
    {
        Request,
        Response, 
        Push, 
        Pull, 
        Publisher,
        Subscriber, 
        Dealer, 
        Router, 
        XPublisher, 
        XSubscriber, 
        Pair
    }

    public class ChannelSocket: IDisposable
    {
        protected NetMQSocket _socket;
        private static int _count = 0;

        public ChannelSocket(Func<NetMQSocket> create, string socketId)
        {
            Contract.Requires(create != null);
            Contract.Requires(socketId != null);
            Contract.Requires(socketId != string.Empty);

            Contract.Ensures(_socket != null);
            Contract.Ensures(_count > 0);

            _socket = create();
            _socket.Options.Identity = Encoding.ASCII.GetBytes(socketId);

            Contract.Assert(_socket != null);
            _count++;

            if (_socket is RequestSocket)
                Type = SocketType.Request;
            else if (_socket is ResponseSocket)
                Type = SocketType.Response;

            else if (_socket is PushSocket)
                Type = SocketType.Push;
            else if (_socket is PullSocket)
                Type = SocketType.Pull;

            else if (_socket is DealerSocket)
                Type = SocketType.Dealer;
            else if (_socket is RouterSocket)
                Type = SocketType.Router;

            else if (_socket is XPublisherSocket)
                Type = SocketType.XPublisher;
            else if (_socket is XSubscriberSocket)
                Type = SocketType.XSubscriber;

            else if (_socket is PublisherSocket)
                Type = SocketType.Publisher;
            else if (_socket is SubscriberSocket)
                Type = SocketType.Subscriber;

            else if (_socket is PairSocket)
                Type = SocketType.Pair;
        }

        public void Bind(IPEndPoint address)
        {
            Contract.Requires(address != null);
            _socket.Bind($"tcp://{address.ToString()}");
        }

        public void Connect(IPEndPoint address)
        {
            Contract.Requires(address != null);
            _socket.Connect($"tcp://{address.ToString()}");
        }


        public IEnumerable<NetMQMessage> Receive(TimeSpan duration)
        {
            Contract.Ensures(Contract.Result<IEnumerable<NetMQMessage>>() != null);
            Contract.Assert(_socket != null, "socket cannot be null");

            var messages = new List<NetMQMessage>();
            var delay = new Delay(duration);

            while (!delay.Elapsed)
            {
                var message = new NetMQMessage();
                if (_socket.TryReceiveMultipartMessage(ref message))
                    messages.Add(message);
                else
                    delay.Elapse();
            }

            return messages;
        }
        public void Send(NetMQMessage[] args)
        {
            Contract.Requires(args != null && args.Length>0);
            Contract.Assert(_socket != null, "socket cannot be null");

            foreach (var msg in args)
                _socket.SendMultipartMessage(msg);
        }
        public void Send(NetMQMessage arg)
        {
            Contract.Requires(arg != null);
            Contract.Assert(_socket != null, "socket cannot be null");

           _socket.SendMultipartMessage(arg);
        }

        public SocketType Type { get; } 

        #region IDisposable Support
        private bool _isDisposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    if (_socket != null)
                    {
                        _socket.Close();
                        _socket.Dispose();
                        //_socket = null;
                    }
                    _count--;

                    if (--_count <= 0)
                    {
                        NetMQConfig.Cleanup();
                        _count = 0;
                    }
                } // managed resources only

                _isDisposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
             GC.SuppressFinalize(this);
        }
        #endregion
    }
}
