﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Net;
using NetworkChannel.Common;
using NetMQ.Sockets;
using System.Text;

namespace NetworkChannel
{
    public class ChannelWriter<TSerializer> : NetworkChannel<TSerializer>
        where TSerializer : IObjectSerializer, new()
    {
        public ChannelWriter(IPEndPoint host, IPEndPoint target):base(host, target)
        {
            Contract.Requires<ArgumentNullException>(host != null);
            Contract.Ensures(_control != null);
            Contract.Ensures(_data != null);

            _control = new ChannelSocket(()=> new DealerSocket(), Host.ToString());
            _control.Connect(Destination.Control);

            _data = new ChannelSocket(()=> new PushSocket(), Host.Data.ToString());
            _data.Connect(Destination.Data);
        }

        public override IEnumerable<DataMessage> ReceiveData(TimeSpan duration)
        {
            throw new InvalidOperationException("Cannot receive data from a NetworkChannelWriter");
        }
        public override void SendSignal(IPEndPoint target, int command,  object[] args)
        {
            base.SendSignal(command, args);
        }
    }
}
