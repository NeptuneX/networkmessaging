﻿using System;
using System.Diagnostics.Contracts;
using System.Net;

namespace NetworkChannel.Common
{
    public class ChannelEndPoint
    {
        public IPEndPoint Control { get; }
        public IPEndPoint Data { get; }

        internal ChannelEndPoint(IPEndPoint endPoint)
        {
            Contract.Requires<ArgumentNullException>(endPoint != null, "valid ip is required");
            Contract.Requires<IndexOutOfRangeException>(endPoint.Port >= 1000 || endPoint.Port <= 65000, "port must be within 1000- 65000");
            Contract.Requires<ArgumentException>(endPoint.Address != IPAddress.None, "valid Ip is required");

            Control = endPoint;
            Data = new IPEndPoint(Control.Address, Control.Port + 1); 
        }

        public override string ToString() => $"[{ControlString}, {DataString}]";
        public string ControlString => $"tcp://{Control.ToString()}";
        public string DataString => $"tcp://{Data.ToString()}";
    }
}
