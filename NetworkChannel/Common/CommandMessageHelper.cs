﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Net;
using System.Text;
using NetMQ;

namespace NetworkChannel.Common
{
    public sealed class CommandMessageHelper
    {
        public static NetMQMessage Create(IPEndPoint sender, int command,  params object[] args)
        {
            Contract.Requires(args != null);
            Contract.Requires<ArgumentException>(sender!=null, "cannot create message for empty sender");
            Contract.Ensures(Contract.Result<NetMQMessage>() != null);

            var frames = new List<NetMQFrame>(args.Length + 1 + 1);

            //[]
            //[sender]
            //[command]
            //[arg count]

            frames.Add(NetMQFrame.Empty);
            frames.Add(new NetMQFrame(sender.ToString()));
            frames.Add(new NetMQFrame(BitConverter.GetBytes(command)));


            if (args.Any())
            {
                frames.Add(new NetMQFrame(BitConverter.GetBytes(args.Length)));
                foreach (var it in args)
                {
                    var bytes = ConvertArgument2Bytes(it);
                    var frame = new NetMQFrame(bytes);
                    frames.Add(frame);
                }
            }
            else
                frames.Add(new NetMQFrame(BitConverter.GetBytes(0)));

            return new NetMQMessage(frames);
        }
        public static CommandMessage Extract(NetMQMessage message)
        {
            Contract.Requires<ArgumentNullException>(message != null && message.FrameCount >= 4, "invalid ZMessage for signal retrievel");

            var ret = CommandMessage.Empty();

            try
            {
                var idx = 0;

                //[]
                //[sender]
                //[command]
                //[arg count]

                //Locate delimeter frame, Packet starts after that
                while (!message[idx++].IsEmpty && idx < message.FrameCount)
                    ;

                if (idx >= message.FrameCount)
                    throw new ArgumentOutOfRangeException("cannot find any signals");



                var sender = IPEndPointEx.Parse(message[idx++].ConvertToString());
                var command = BitConverter.ToInt32(message[idx++].Buffer, 0);
                var count = BitConverter.ToInt32(message[idx++].Buffer, 0);

                if (count > 0)
                {
                    var data = new List<object>(count);
                    for (var i = 0; (i < count) && (idx < message.FrameCount); i++)
                    {
                        var argument = ConvertBytes2Argument(message[idx].Buffer);
                        data.Add(argument);
                        idx += 1;
                    }
                    ret = new CommandMessage(sender, command, data.ToArray());
                }
                else
                    ret = new CommandMessage(sender, command);
            }
            finally
            {

            }

            return ret;
        }

        static byte[] ConvertArgument2Bytes(object it)
        {
            //determine type
            var type = ValueType.Unknown;
            var dataBytes = (byte[]) null;

            if (it is short)
            {
                type = ValueType.Short;
                dataBytes = BitConverter.GetBytes((short) it);
            }
            else if (it is int)
            {
                type = ValueType.Int;
                dataBytes = BitConverter.GetBytes((int) it);
            }
            else if (it is long)
            {
                type = ValueType.Long;
                dataBytes = BitConverter.GetBytes((long) it);
            }

            else if (it is float)
            {
                type = ValueType.Float;
                dataBytes = BitConverter.GetBytes((float) it);
            }
            else if (it is double)
            {
                type = ValueType.Double;
                dataBytes = BitConverter.GetBytes((double) it);
            }

            else if (it is bool)
            {
                type = ValueType.Boolean;
                dataBytes = BitConverter.GetBytes((bool) it);
            }
            else if (it is char)
            {
                type = ValueType.Character;
                dataBytes = BitConverter.GetBytes((char) it);
            }
            else if (it is string)
            {
                type = ValueType.String;
                dataBytes = ASCIIEncoding.ASCII.GetBytes((string) it);
            }
            else if (it is DateTime)
            {
                type = ValueType.DateTime;
                dataBytes = BitConverter.GetBytes(((DateTime) it).ToBinary());
            }
            else if (it is TimeSpan)
            {
                type = ValueType.TimeSpan;
                dataBytes = BitConverter.GetBytes(((TimeSpan) it).Ticks);
            }

            var typeBytes = BitConverter.GetBytes((int) type);

            var ret = new byte[typeBytes.Length + dataBytes.Length];

            Buffer.BlockCopy(typeBytes, 0, ret, 0, typeBytes.Length);
            Buffer.BlockCopy(dataBytes, 0, ret, typeBytes.Length, dataBytes.Length);

            return ret;
        }
        static object ConvertBytes2Argument(byte[] bytes)
        {
            Contract.Requires(bytes != null);

            var type = (ValueType) BitConverter.ToInt32(bytes, 0);
            var size = bytes.Length - 4;
            object argument = null;
            var data = new byte[size];
            Buffer.BlockCopy(bytes, 4, data, 0, size);
            switch (type)
            {
                case ValueType.Short:
                    argument = BitConverter.ToInt16(data, 4);
                    break;

                case ValueType.Int:
                    argument = BitConverter.ToInt32(data, 0);
                    break;

                case ValueType.Long:
                    argument = BitConverter.ToInt64(data, 0);
                    break;

                case ValueType.Float:
                    argument = BitConverter.ToSingle(data, 0);
                    break;

                case ValueType.Double:
                    argument = BitConverter.ToDouble(data, 0);
                    break;

                case ValueType.Boolean:
                    argument = BitConverter.ToBoolean(data, 0);
                    break;
                case ValueType.Character:
                    argument = BitConverter.ToChar(data, 0);
                    break;
                case ValueType.String:
                    argument = ASCIIEncoding.ASCII.GetString(data, 0, data.Length);
                    break;
                case ValueType.DateTime:
                    argument = DateTime.FromBinary(BitConverter.ToInt64(data, 0));
                    break;
                case ValueType.TimeSpan:
                    argument = TimeSpan.FromTicks(BitConverter.ToInt64(data, 0));
                    break;
            }

            return argument;
        }
    }
}
