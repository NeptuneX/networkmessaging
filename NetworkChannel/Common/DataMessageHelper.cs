﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Net;
using System.Text;
using NetMQ;

namespace NetworkChannel.Common
{
    public class DataMessageHelper<TSerializer>
        where TSerializer: IObjectSerializer, new()
    {
        readonly IObjectSerializer _serializer;

        public DataMessageHelper()
        {
            _serializer = new TSerializer();
        }
        public NetMQMessage Create(IPEndPoint sender, string magicId,  object[] args)
        {
            Contract.Requires<ArgumentException>(sender != null, "cannot create message for empty sender");
            Contract.Requires(args != null);
            Contract.Requires<ArgumentNullException>(args.Length > 0, "cannot send empty data packets");
            Contract.Ensures(Contract.Result<NetMQMessage>() != null);


            var frames = new List<NetMQFrame>(args.Length + 1 + 1);

            frames.Add(new NetMQFrame(sender.ToString()));
            frames.Add(new NetMQFrame(magicId));
            frames.Add(new NetMQFrame(BitConverter.GetBytes(args.Length)));

            var data = _serializer.ToBytes(args);
            frames.Add(new NetMQFrame(data));
            //foreach (var it in args)
            //{
            //    frames.Add(frame);
            //}

            return new NetMQMessage(frames);
        }
        public DataMessage Extract(NetMQMessage message)
        {
            Contract.Requires<ArgumentNullException>(message != null && message.FrameCount >= 2, "invalid ZMessage for data retrievel");

            var ret = DataMessage.Empty();

            try
            {

                IPEndPoint sender = IPEndPointEx.Parse(message.Pop().ConvertToString());
                var magicId = message.Pop().ConvertToString();
                var count = BitConverter.ToInt32(message.Pop().Buffer, 0);

                //var data = new List<object>(count);
                var frame = message.Pop();
                var data = _serializer.ToObjects<object>(frame.Buffer);
                //foreach (var frame in message)
                //{
                //    var obj = _serializer.ToObject<object>(frame.Buffer);
                //    data.Add(obj);
                //}

                ret = new DataMessage(sender, magicId, data);
            }
            catch
            {
                ret = DataMessage.Empty();
            }

            return ret;
        }
     }
}
