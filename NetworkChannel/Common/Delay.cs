﻿using System;
using System.Diagnostics.Contracts;

namespace NetworkChannel.Common
{
    public class Delay
    {
        readonly long _duration;
        readonly long _startTick;
        bool _IsPassed;

        public Delay(TimeSpan duration)
        {
            Contract.Requires<ArgumentException>(duration != TimeSpan.Zero && duration < TimeSpan.MaxValue, "Invalid duration for delay");

            _duration = duration.Ticks;
            _startTick = DateTime.UtcNow.Ticks;
            _IsPassed = false;
        }
        public bool Elapsed => _IsPassed || ( DateTime.UtcNow.Ticks - _startTick)  >= _duration;
        public void Elapse() => _IsPassed = true;
    }
}
