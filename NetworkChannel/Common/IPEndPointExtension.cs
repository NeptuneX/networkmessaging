﻿using System;
using System.Globalization;
using System.Net;

namespace NetworkChannel.Common
{
    public static class IPEndPointEx
    {
        public static IPEndPoint Parse(this IPEndPoint self, string txt)
        {
            return Parse(txt);
        }
        public static IPEndPoint Parse(string txt)
        {
            IPEndPoint ret = new IPEndPoint(IPAddress.Loopback, 1);

            string[] ep = txt.Split(':');
            if (ep.Length < 2) throw new FormatException("Invalid endpoint format");
            IPAddress ip;
            if (ep.Length > 2)
            {
                if (!IPAddress.TryParse(string.Join(":", ep, 0, ep.Length - 1), out ip))
                {
                    throw new FormatException("Invalid ip-adress");
                }
            }
            else
            {
                if (!IPAddress.TryParse(ep[0], out ip))
                {
                    throw new FormatException("Invalid ip-adress");
                }
            }
            int port;
            if (!int.TryParse(ep[ep.Length - 1], NumberStyles.None, NumberFormatInfo.CurrentInfo, out port))
            {
                throw new FormatException("Invalid port");
            }
            ret = new IPEndPoint(ip, port);


            return ret;
        }

        public static IPEndPoint Empty(this IPEndPoint self)
        {
            return new IPEndPoint(IPAddress.Loopback, 1);
        }
    }
}
