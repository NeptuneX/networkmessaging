﻿using System;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Net;
using NetworkChannel.Common;


namespace NetworkChannel
{
    [Serializable]
    public class Message
    {
        public IPEndPoint Sender { get;  }
        protected readonly Object[] _arguments;

        protected Message(IPEndPoint sentby, object[] args)
        {
            //Contract.Requires(sentby!=string.Empty); if it is enabled, empty message wont work
            Contract.Requires(sentby != null);
            Contract.Requires(args != null);

            Sender = sentby;
            _arguments = new object[args.Length];
            if (args.Length > 0)
                Array.ConstrainedCopy(args, 0, _arguments, 0, args.Length);
        }
        protected Message()
        {
            Sender = Sender.Empty();
            _arguments = new object[0];
        }
        protected object this[int idx]
        {
            get
            {
                Contract.Requires(idx>=0);
                Contract.Assert(idx < _arguments.Length, "invalid index requested");

                return _arguments[idx];
            }
        }
        public int Length => _arguments.Length;
    }
   




    public class CommandMessage : Message
    {
        public int Command { get; }
        public object[] Arguments => _arguments;


        public CommandMessage(IPEndPoint sender, int command,  object[] args) : base(sender, args)
        {
            Contract.Requires(sender != null);
            //Contract.Requires(command >= 0);    a program can use negative numbers for communication.

            Command = command;
        }
        public CommandMessage(IPEndPoint sender, int command) : base(sender, new object[0])
        {
            Contract.Requires(sender != null);
            //Contract.Requires(command >= 0);    a program can use negative numbers for communication.

            Command = command;
        }
        private CommandMessage():base()
        {
        }

        

        public static CommandMessage Empty()
        {
            Contract.Ensures(Contract.Result<CommandMessage>()!=null);

            return new CommandMessage();
        }
    }
   
    public class DataMessage : Message
    {
        public string Key { get; }
        public object[] Data => _arguments;

        public DataMessage(IPEndPoint sender, string key,  object[] args) : base(sender, args)
        {
            Contract.Requires(sender != null);
            Key = key;
        }
        private DataMessage() : base() { }


        public static DataMessage Empty()
        {
            Contract.Ensures(Contract.Result<DataMessage>()!=null);

            return new DataMessage();
        }
    }
} 