﻿

namespace NetworkChannel.Common
{
    internal enum ValueType
    {
        Unknown = 0,

        Short = 1,
        Int = 2,
        Long = 3,

        Float = 11,
        Double = 12,

        Boolean = 21,

        Character = 31,
        String = 32,

        DateTime = 41,
        TimeSpan = 42

    }
}
