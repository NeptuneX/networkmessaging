using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Net;

namespace NetworkChannel
{
    [ContractClassFor(typeof(INetworkChannel))]
    internal abstract class INetworkChannel_Contract : INetworkChannel
    {
        public IEnumerable<CommandMessage> ReceiveSignal(TimeSpan duration)
        {
            Contract.Requires(duration>TimeSpan.MinValue && duration <TimeSpan.MaxValue);

            return default(IEnumerable<CommandMessage>);
        }
        public IEnumerable<DataMessage> ReceiveData(TimeSpan duration)
        {
            Contract.Requires(duration > TimeSpan.MinValue && duration < TimeSpan.MaxValue);

            return default(IEnumerable<DataMessage>);
        }
        public void SendSignal(int command,  object[] args)
        {
            //Code contract checks here...
        }
        public string SendData(object[] args)
        {
            Contract.Requires(args != null);
            Contract.Ensures(Contract.Result<string>() != "");

            return default(string);
        }
        public void SendSignal(IPEndPoint target, int command,  object[] args)
        {
            Contract.Requires(target != null);
        }
        public void SendData(string magicId, object[] args)
        {
            Contract.Requires(magicId != null);
            Contract.Requires(args != null);


        }

        void IDisposable.Dispose()
        {
            // just to enable compilation
        }
    }
}