using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Net;

namespace NetworkChannel
{
    [ContractClass(typeof (INetworkChannel_Contract))]
    public interface INetworkChannel : IDisposable
    {
        IEnumerable<CommandMessage> ReceiveSignal(TimeSpan duration);
        IEnumerable<DataMessage> ReceiveData(TimeSpan duration);

        void SendSignal(int command,  object[] args);
        string SendData(object[] args);

        void SendSignal(IPEndPoint target, int command,  object[] args);
        void SendData(string magicId, object[] args);
    }
}