using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
namespace NetworkChannel
{
    [ContractClassFor(typeof(INetworkDataChannel))]
    internal abstract class INetworkDataChannel_Contract : INetworkDataChannel
    {
        public void Dispose()
        {
        }

        public IEnumerable<DataMessage> ReceiveData(TimeSpan duration)
        {
            return default(IEnumerable<DataMessage>);
        }
        public string SendData(object[] args)
        {
            //Code contract checks here...
            return default(string);
        }
        public void SendData(string magicId, object[] args)
        {
            //Code contract checks here...
        }
    }
}