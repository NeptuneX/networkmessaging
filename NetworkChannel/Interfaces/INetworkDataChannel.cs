using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;

namespace NetworkChannel
{
    [ContractClass(typeof (INetworkDataChannel_Contract))]
    public interface INetworkDataChannel : IDisposable
    {
        IEnumerable<DataMessage> ReceiveData(TimeSpan duration);
        string SendData(object[] args);
        void SendData(string magicId, object[] args);
    }
}