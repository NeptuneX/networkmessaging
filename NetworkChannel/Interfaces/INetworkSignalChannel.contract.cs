using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics.Contracts;
using System.Net;

namespace NetworkChannel
{
    [ContractClassFor(typeof(INetworkSignalChannel))]
    internal abstract class INetworkSignalChannel_Contract : INetworkSignalChannel
    {
        public void Dispose()
        {
        }

        public IEnumerable<CommandMessage> ReceiveSignal(TimeSpan duration)
        {
            Contract.Ensures(Contract.Result<IEnumerable<CommandMessage>>()!=null);

            return default(IEnumerable<CommandMessage>);
        }

        public void SendSignal(int command,  params object[] args)
        {
            Contract.Requires(args != null);
        }
        public void SendSignal(IPEndPoint target, int command,  params object[] args)
        {
            Contract.Requires(target != null);
            Contract.Requires(args != null);
        }
    }
}