using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics.Contracts;
using System.Net;

namespace NetworkChannel
{
    [ContractClass(typeof (INetworkSignalChannel_Contract))]
    interface INetworkSignalChannel:IDisposable
    {
        IEnumerable<CommandMessage> ReceiveSignal(TimeSpan duration);
        void SendSignal(int command,  params object[] args);
        void SendSignal(IPEndPoint target, int command,  params object[] args);
        //void SendSignal(int command);
        //void SendSignal(IPEndPoint target, int command);
    }
}