using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics.Contracts;

namespace NetworkChannel.Common
{
    [ContractClassFor(typeof(IObjectSerializer))]
    internal abstract class IObjectSerializer_Contract : IObjectSerializer
    {
        private const int MB = 1048576;

        public byte[] ToBytes<T>(T[] data) where T : class
        {
            Contract.Requires<ArgumentNullException>(data != null, "null objects cannot be serialized");
            Contract.Ensures(Contract.Result<byte[]>().Length < 10 * MB);
            return default(byte[]);
        }

        public byte[] ToBytes<T>(T data) where T : class
        {
            Contract.Requires<ArgumentNullException>(data != null, "null objects cannot be serialized");
            Contract.Ensures(Contract.Result<byte[]>().Length < 10 * MB);
            return default(byte[]);
        }

        public T ToObject<T>(byte[] arrBytes) where T : class
        {
            Contract.Requires<ArgumentNullException>(arrBytes != null && arrBytes.Length > 0, "bytes cannot be empty to deserialize an object");
            Contract.Ensures(Contract.Result<T>() != null);

            return default(T);
        }

        public T[] ToObjects<T>(byte[] arrBytes) where T : class
        {
            Contract.Requires<ArgumentNullException>(arrBytes != null && arrBytes.Length > 0, "bytes cannot be empty to deserialize an object");
            Contract.Ensures(Contract.Result<T[]>() != null);

            return default(T[]);
        }
    }
}