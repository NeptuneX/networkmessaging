﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics.Contracts;

namespace NetworkChannel.Common
{
    [ContractClass(typeof (IObjectSerializer_Contract))]
    public interface IObjectSerializer
    {
        byte[] ToBytes<T>(T data)where T : class;
        byte[] ToBytes<T>(T[] data) where T : class;
        T ToObject<T>(byte[] arrBytes)where T : class;
        T[] ToObjects<T>(byte[] arrBytes) where T : class;
    }
}