﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Net;
using NetworkChannel.Common;
using NetMQ;
using NetMQ.Sockets;

namespace NetworkChannel
{ 
    public class NetworkChannel<TSerializer>: INetworkChannel
        where TSerializer : IObjectSerializer, new()
    {
        protected ChannelSocket _control;
        protected ChannelSocket _data;

       
        readonly DataMessageHelper<TSerializer> _serializer;

        //private static int _count =0;

        public NetworkChannel(IPEndPoint point)
        {
            Contract.Requires(point!=null);

            _serializer =  new DataMessageHelper<TSerializer>();
            
            Host = new ChannelEndPoint(point);
            Destination = null;

            _control = null;
            _data = null;
            //_count++;
        }
        public NetworkChannel(IPEndPoint point, IPEndPoint target): this(point)
        {
            Contract.Requires(point != null);

            Destination = new ChannelEndPoint(target);
        }


        public ChannelEndPoint Host { get; }
        public ChannelEndPoint Destination { get; }


        //protected IEnumerable<NetMQMessage> ReceiveFromSocket(NetMQSocket socket, TimeSpan duration)
        //{
        //    Contract.Ensures(Contract.Result<IEnumerable<NetMQMessage>>()!=null);

        //    var messages = new List<NetMQMessage>();
        //    var delay = new Delay(duration);
  
        //    while (!delay.Elapsed)
        //    {
        //        var message = new NetMQMessage();
        //        if (socket.TryReceiveMultipartMessage(ref message))
        //            messages.Add(message);
        //        else
        //            delay.Elapse();
        //    }

        //    return messages;
        //}
        //protected void SendFromSocket(NetMQSocket socket, params NetMQMessage[] args)
        //{
        //    Contract.Requires(socket != null);
        //    Contract.Requires(args != null);

        //    foreach(var msg in args)
        //    {
        //        socket.SendMultipartMessage(msg);
        //    }
        //}


        #region INetworkChannel
        public virtual IEnumerable<CommandMessage> ReceiveSignal(TimeSpan duration)
        {
            var commands = new List<CommandMessage>();
            foreach (var it in _control.Receive(duration))
            {
                commands.Add(CommandMessageHelper.Extract(it));
            }

            return commands;
        }
        public virtual IEnumerable<DataMessage> ReceiveData(TimeSpan duration)
        {
            var packets = new List<DataMessage>();
            foreach (var it in _data.Receive(duration))
            {
                packets.Add(_serializer.Extract(it));
            }

            return packets;
        }

        public virtual void SendSignal(int command,  object[] args)
        {
            //SendSignal(string.Empty, command, args);
            var msg = CommandMessageHelper.Create(Host.Control, command, args);
            _control.Send(msg);
        }
        public virtual void SendSignal(IPEndPoint target, int command,  object[] args)
        {
            var msg = CommandMessageHelper.Create(Host.Control, command, args);

            if (target!=null)
                msg.Push(target.ToString()); // msg.Insert(0, new ZFrame(target));// to make it work from the router
            _control.Send(msg);
        }

        public virtual string SendData(object[] args)
        {
            var tmpId = Guid.NewGuid().ToString();
            SendData(tmpId, args);

            return tmpId;
        }
        public virtual void SendData(string magicId, object[] args)
        {
            var msg = _serializer.Create(Host.Control, magicId, args);
            _data.Send(msg);
        }

        public void Dispose()
        {
            _control.Dispose();
            _data.Dispose();
        }

        #endregion
    }
}
