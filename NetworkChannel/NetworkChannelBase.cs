﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace NetworkChannel
{
    public class NetworkChannelBase: IDisposable
    {
        protected ChannelSocket _socket;

        public NetworkChannelBase(IPEndPoint host)
        {
            Host = host;
        }
        public NetworkChannelBase(IPEndPoint host, IPEndPoint target):this(host)
        {
            Destination = target;
        }

        public IPEndPoint Host { get; }
        public IPEndPoint Destination { get; }

        public void Dispose()
        {
            if (_socket != null)
                _socket.Dispose();
        }
    }
}
