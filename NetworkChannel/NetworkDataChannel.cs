﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Net;
using NetMQ.Sockets;
using NetworkChannel.Common;

namespace NetworkChannel
{
    public class NetworkDataChannel<TSerializer>: 
        NetworkChannelBase,
        INetworkDataChannel
        where TSerializer : IObjectSerializer, new()
    {
        readonly DataMessageHelper<TSerializer> _serializer;

        public NetworkDataChannel(IPEndPoint host) : base(host)
        {
            Contract.Requires(host != null);
            Contract.Ensures(_socket != null);

            _serializer = new DataMessageHelper<TSerializer>();
            _socket = new ChannelSocket(() => new PullSocket(), Host.ToString());
            _socket.Bind(Host);
        }
        public NetworkDataChannel(IPEndPoint host, IPEndPoint target = null):base(host, target)
        {
            Contract.Requires(host != null);
            Contract.Ensures(_socket != null);

            _serializer = new DataMessageHelper<TSerializer>();
            _socket = new ChannelSocket(() => new PushSocket(), Host.ToString());
            _socket.Connect(Destination);
        }

        #region INetworkDataChannel
        public virtual IEnumerable<DataMessage> ReceiveData(TimeSpan duration)
        {
            if (_socket.Type == SocketType.Push)
                throw new Exception("Cannot receive data from a push socket");

            var packets = new List<DataMessage>();

            var messages = _socket.Receive(duration);
            foreach (var it in messages)
                packets.Add(_serializer.Extract(it));

            return packets;
        }
        public virtual string SendData(object[] args)
        {
            var key = Guid.NewGuid().ToString();

            SendData(key, args);

            return key;
        }
        public virtual void SendData(string key, object[] args)
        {
            Contract.Assert(_socket.Type != SocketType.Pull, "Cannot send data from a pull socket");

            var msg = _serializer.Create(Host, key, args);
            _socket.Send(msg);
        }
        #endregion
    }
}
