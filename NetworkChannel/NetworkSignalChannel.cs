﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Net;
using NetworkChannel.Common;
using NetMQ;
using NetMQ.Sockets;
using System.Text;

namespace NetworkChannel
{ 
    public class NetworkSignalChannel: 
        NetworkChannelBase,
        INetworkSignalChannel
    {
        public NetworkSignalChannel(IPEndPoint point):base(point)
        {
            Contract.Requires(point!=null);
            Contract.Ensures(_socket!=null);

            _socket = new ChannelSocket(()=> new RouterSocket(), Host.ToString());
            _socket.Bind(Host);
        }
        public NetworkSignalChannel(IPEndPoint host, IPEndPoint target):base(host, target)
        {
            Contract.Requires(host != null);
            Contract.Requires(target != null);
            Contract.Ensures(_socket != null);

            _socket = new ChannelSocket(() => new DealerSocket(), Host.ToString());
            _socket.Connect(Destination);
        }

        #region INetworkSignalChannel
        public virtual IEnumerable<CommandMessage> ReceiveSignal(TimeSpan duration)
        {
            var commands = new List<CommandMessage>();
            var messages = _socket.Receive(duration);
            foreach (var it in messages)
                commands.Add(CommandMessageHelper.Extract(it));

            return commands;
        }
        public virtual void SendSignal(int command, params object[] args)
        {
            var msg = CommandMessageHelper.Create(Host, command, args);
            _socket.Send(msg);
        }
        public virtual void SendSignal(IPEndPoint target, int command, params object[] args)
        {
            var msg = CommandMessageHelper.Create(Host, command, args);

            if (target != null)
                msg.Push($"{target.ToString()}"); // to make it work from the router
            _socket.Send(msg);
        }
        //public void SendSignal(int command)
        //{
        //    var msg = CommandMessageHelper.Create(Host, command);
        //    _socket.Send(msg);
        //}
        //public void SendSignal(IPEndPoint target, int command)
        //{
        //    var msg = CommandMessageHelper.Create(Host, command);

        //    if (target!=null)
        //        msg.Push($"{target.ToString()}"); // to make it work from the router
        //    _socket.Send(msg);
        //}

        #endregion
    }
}
