﻿using System;
using System.IO;
using NetworkChannel.Common;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;

namespace NetworkMessaging.Common
{
    public class BSONSerializer : IObjectSerializer
    {
        public byte[] ToBytes<T>(T[] data) where T : class
        {
            throw new NotImplementedException();
        }

        public byte[] ToBytes<T>(T obj) where T : class
        {
            byte[] ret = null;
            using (var ms = new MemoryStream())
            {
                using (var writer = new BsonWriter(ms))
                {
                    var serializer = new JsonSerializer();
                    serializer.Serialize(writer, obj);
                }
                ret = ms.ToArray();
            }

            return ret;
        }

        public T ToObject<T>(byte[] bytes) where T : class
        {
            var ret = default(T);
            using (var ms = new MemoryStream(bytes))
            {
                using (var reader = new BsonReader(ms))
                {
                    var serializer = new JsonSerializer();
                    ret = serializer.Deserialize<T>(reader);
                }
            }
            return ret;
        }

        public T[] ToObjects<T>(byte[] arrBytes) where T : class
        {
            throw new NotImplementedException();
        }
    }
}
