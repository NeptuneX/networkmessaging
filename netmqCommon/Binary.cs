﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace netmqCommon
{
    public static class Binary
    {
        public static byte[] To<T>(T data)
        {
            byte[] arr;

            if (data == null)
                return null;

            BinaryFormatter bf = new BinaryFormatter();
            using (var ms = new MemoryStream())
            {
                bf.Serialize(ms, data);
                arr = ms.ToArray();
            }

            return arr;
        }

        public static T From<T>(byte[] arrBytes)
        {
            T ret = default(T);
            using (var memStream = new MemoryStream())
            {
                BinaryFormatter binForm = new BinaryFormatter();
                memStream.Write(arrBytes, 0, arrBytes.Length);
                memStream.Seek(0, SeekOrigin.Begin);
                Object obj = (Object) binForm.Deserialize(memStream);
                ret = (T) Convert.ChangeType(obj, typeof(T));
            }
            return ret;
        }
    }
}
