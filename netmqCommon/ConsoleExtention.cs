﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetworkMessaging.Common
{
    public static class Keyboard
    {
        [System.Diagnostics.DebuggerStepThrough()]
        public static ConsoleKeyInfo KeyPressed() => Console.KeyAvailable ? Console.ReadKey() : new ConsoleKeyInfo(char.MinValue, ConsoleKey.NoName, false, false, false);


        public static T Ask<T>(string prompt)
        {
            var ret = default(T);

            Console.Write($"{prompt}:");

            var tmp = Console.ReadLine();

            ret = (T)Convert.ChangeType(tmp, typeof(T));

            return ret;
        }

    }
}
