﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using NetworkChannel.Common;

namespace NetworkMessaging.Common
{
    public class QuickBinarySerializer : IObjectSerializer
    {
        BinaryFormatter _formatter;
        SerializationBinder _binder;
        MemoryStream _stream;

        public QuickBinarySerializer()
        {
            _binder = new TypeResolver();
            _formatter = new BinaryFormatter();
            _formatter.Binder = _binder;

            _stream = new MemoryStream(10485760);
        }
        public byte[] ToBytes<T>(T data) where T : class
        {
            byte[] ret = null;

            _stream.Position = 0;
            _formatter.Serialize(_stream, data);
            ret = _stream.ToArray();

            return ret;
        }
        public byte[] ToBytes<T>(T[] data) where T : class
        {
            byte[] ret = null;

            _stream.Position = 0;
            _formatter.Serialize(_stream, data);
            ret = _stream.ToArray();

            return ret;
        }

        public T ToObject<T>(byte[] arrBytes) where T : class
        {
            T obj = null;

           using (var ms = new MemoryStream(arrBytes, 0, arrBytes.Length))
            {
                obj = (T)_formatter.Deserialize(ms);
            }

            return obj;
        }
        public T[] ToObjects<T>(byte[] arrBytes) where T : class
        {
            T[] obj = null;

            using (var ms = new MemoryStream(arrBytes, 0, arrBytes.Length))
            {
                obj = (T[]) _formatter.Deserialize(ms);
            }

            return obj;
        }
    }

    sealed class TypeResolver : SerializationBinder
    {
        Dictionary<string, Type> _types = new Dictionary<string, Type>(10);
        public override Type BindToType(string assemblyName, string typeName)
        {
            Type typeToDeserialize = null;

            if (_types.ContainsKey(typeName))
                typeToDeserialize = _types[typeName];
            else
            {
                assemblyName = assemblyName.Split(',')[0];

                // For each assemblyName/typeName that you want to deserialize to
                // a different type, set typeToDeserialize to the desired type.
                foreach (var asm in AppDomain.CurrentDomain.GetAssemblies())
                {
                    if (string.Compare(asm.GetName().Name, assemblyName, true) == 0)
                    {
                        foreach (var it in asm.GetTypes())
                        {
                            if (string.Compare(it.FullName, typeName, true) == 0)
                            {
                                typeToDeserialize = it;
                                _types.Add(typeName, it);
                                break;
                            }
                        }
                        break;
                    }
                }
            }

            return typeToDeserialize;
        }
    }
}
