﻿using System;

namespace netmqCommon
{
    public interface IRequestResponseSocket<T>: IDisposable
    {
        T Receive();
        void Send(T data);
    }
}