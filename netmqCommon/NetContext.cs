﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using zmq = ZeroMQ;

namespace netmqCommon
{
    public class NetContext: IDisposable
    {
        protected static zmq.ZContext _ctx;
        protected zmq.ZError _error;


        public NetContext()
        {
            _ctx = zmq.ZContext.Create();
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _ctx.Dispose();
                    _ctx = null;
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        ~NetContext()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }


    public class RequestResponseBase<T> : NetContext, IRequestResponseSocket<T>
    {
        protected readonly IPEndPoint _point;
        protected readonly NetTransportType _protocol;
        protected readonly string _address;
        protected zmq.ZSocket _socket;

        public RequestResponseBase(IPEndPoint point, NetTransportType type)
        {
            _point = point;
            _protocol = type;
            _address = NetworkHelper.GetZeroMQAddress(_point, _protocol);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _socket.Disconnect(_address, out _error);
                _socket.Close(out _error);
                _socket.Dispose();
            }
            base.Dispose(disposing);
        }

        public T Receive()
        {
            T ret = default(T);
            var msg = new zmq.ZMessage();
            _socket.ReceiveMessage(ref msg, zmq.ZSocketFlags.DontWait, out _error);
            if (msg.Count > 0 && _error == null)
            {

                byte[] arr = new byte[msg[0].Length];
                var tmp = msg[0].Read(arr, 0, arr.Length);

                ret = Binary.From<T>(arr);
            }
            return ret;
        }
        public void Send(T data)
        {
            var bytes = Binary.To(data);
            var msg = new ZeroMQ.ZMessage();
            msg.Add(new zmq.ZFrame(bytes, 0, bytes.Length));

            _socket.Send(msg, out _error);
        }

    }
    public class RequestResponseServer<T>: RequestResponseBase<T>
    {
        //public RequestResponseServer(string address, int port): base(address, port)
        //{
        //    _socket = zmq.ZSocket.Create(_ctx, zmq.ZSocketType.REP, out _error);
        //    _socket.Bind(_address, out _error);
        //}

        public RequestResponseServer(IPEndPoint end, NetTransportType protocol) : base(end, protocol)
        {
            _socket = zmq.ZSocket.Create(_ctx, zmq.ZSocketType.REP, out _error);
            _socket.Bind(_address, out _error);
        }
    }

    public class RequestResponseClient<T> : RequestResponseBase<T>
    {
        //public RequestResponseClient(string address, int port) : base(address, port)
        //{
        //    _socket = zmq.ZSocket.Create(_ctx, zmq.ZSocketType.REQ, out _error);
        //    _socket.Connect(_address, out _error);
        //}
        public RequestResponseClient(IPEndPoint end, NetTransportType protocol) : base(end, protocol)
        {
            _socket = zmq.ZSocket.Create(_ctx, zmq.ZSocketType.REQ, out _error);
            _socket.Connect(_address, out _error);
        }
    }


}
