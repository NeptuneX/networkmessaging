﻿namespace netmqCommon
{
    public enum NetTransportType
    {
        Tcp = 1, 
        Udp, 
        InterProcess, 
        InProcess
    }
}