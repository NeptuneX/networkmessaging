﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace netmqCommon
{
    public static class NetworkHelper
    {
        const string PATH_FORMAT = @"{0}://{1}";
        public static string GetZeroMQAddress(IPEndPoint end, NetTransportType type)
        {
            var protocol = Enum.GetName(type.GetType(), type).ToLower();
            return string.Format(PATH_FORMAT, protocol, end.ToString());
        }

        public static IPAddress MyIPAddress
        {
            get 
            {
                return
                    Dns.GetHostAddresses(Dns.GetHostName())
                    .ToList()
                    .Where(x => x.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                    .FirstOrDefault();
              
            }
        }

    }
}
