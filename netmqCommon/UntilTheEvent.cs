﻿using System;

namespace NetworkMessaging.Common
{
    public class UntilTheEvent
    {
        bool _happened;
        readonly Func<bool> _code;

        public UntilTheEvent(Func<bool> code)
        {
            _happened = false;
            _code = code;
        }

        public bool Happened
        {
            get
            {
                if (!_happened)
                {
                    _happened = _code();
                }
                return _happened;
            }
        }
        public void Reset() => _happened = false;
        
    }
}
