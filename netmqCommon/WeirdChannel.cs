﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using NetMQ;
using NetMQ.Sockets;
using NetworkChannel;
using NetworkChannel.Common;

namespace NetworkMessaging.Common
{
    public class SignalEventArgs: EventArgs
    {
        public IEnumerable<CommandMessage> Messages { get; }
        public int Count { get; } 
        public SignalEventArgs(IEnumerable<CommandMessage> arg):base()
        {
            Messages = arg;
            Count = arg.Count();
        }
    }

    public class WeirdChannel: NetworkChannelBase
    {
        public event EventHandler<SignalEventArgs> SignalReceived;
        public WeirdChannel(IPEndPoint host):base(host)
        {
            _socket = new ChannelSocket(() => new RouterSocket(), host.ToString());
            _socket.Bind(host);
        }

        public void Connect(IPEndPoint target)
        {
            _socket.Connect(target);
        }

        public void Send(int cmd, params object[] args)
        {
            var msg = NetworkChannel.Common.CommandMessageHelper.Create(Host, cmd, args);
            _socket.Send(msg);
        }
        public void Send(IPEndPoint pt, int cmd, params object[] args)
        {
            var msg = NetworkChannel.Common.CommandMessageHelper.Create(Host, cmd, args);
            msg.Push(new NetMQFrame(pt.ToString()));
            _socket.Send(msg);
        }
        public void Receive()
        {
            var list = new List<CommandMessage>();

            var tmp= _socket.Receive(TimeSpan.FromMilliseconds(500)).ToList();
            tmp.ForEach(x => list.Add(CommandMessageHelper.Extract(x)));

            if (list.Count > 0 && SignalReceived != null)
                SignalReceived(this, new SignalEventArgs(list));
        }


    }
}
